using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class rotate : MonoBehaviour
{
	
/*
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime);
    }
*/
    public GameObject player;

    private Vector3 _angles;

    void Start()
    {
        // Reuse rather than creating this every update.
        _angles = new Vector3(0.0f, 1.0f, 0.0f);

        //player = GetComponent<GameObject>();
    }

    void FixedUpdate()
    {
        player = GameObject.Find("JohnLemon");

        // Calculate vector from pickup to player.
        Vector3 d = player.transform.position - transform.position;

        //Vector3 d = new Vector3(0.0f, 10.0f, 0.0f);

        // Normalize to a direction.
        d.Normalize();

        // Could use Unity's function, but this will normalize both vectors unnecessarily.
//        float angle = Vector3.Angle(Vector3.forward, d);

        // Could also define our own forward vector, but allocations have cost.
//        float angle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(new Vector3(0.0f, 0.0f, 1.0f), d));

        float angle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(Vector3.forward, d));

        // Textbook, but *not* most efficient solution.
        Vector3 cross = Vector3.Cross(Vector3.forward, d);
        if (cross.y < 0.0f) {
            angle = -angle;
        }

        _angles.y = angle;
        transform.eulerAngles = _angles;
    }
}
