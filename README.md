# CIS410 - gp: Assignment 2 #

## Part 1: John Lemon's Haunted Jaunt 3D Beginner

- Website: [https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner](https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner)

- Implementations: 
    - Player John Lemon
    - Ghosts
    - Gargoyles
    - Environment
    - Audios
    - Virtual Camera
    - Texts
    
## Part 2: Adding Dot Product

- Adding dot product in three (all) gargolyes. Let them always face to the John Lemon whatever the position for the John.

## Part 3: Adding Linear Interpolation

- Adding linear interpolation between the destination and John Lemon. This is the Hint for the game. If we open it, 
  there is a white vase between the destination and John Lemon, and it will make the player know the direction for 
  the destination.
  
- Modified hit object direction. Added ability, when player is moving, the hit will disable.

## Part 4: Adding Particle Effect

- For Xingzhi: Adding the particle effect at the destination to let the player know he got the right place.
- For Liwei: Adding the particle effect on the ghost which in the bathroom.

## Team members:

- Liwei Fan    Email: `liweif@uoregon.edu`

- Xingzhi Wang Email: `xingzhiw@uoregon.edu`

## Contributions:

- Liwei: Part1, Part4; Modified Part 3
- Xingzhi: Part2, Part3, Part4
           
